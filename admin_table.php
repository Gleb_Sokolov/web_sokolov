<?php
include("app/controlers/list.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>MusoTeka</title>
    <link rel="stylesheet" href="styles/style.css">
    <link rel="stylesheet" href="styles/skin.css">
    <link rel="preconnect" href="https://fonts.googleapis.com"><link rel="preconnect" href="https://fonts.gstatic.com" crossorigin><link href="https://fonts.googleapis.com/css2?family=Exo+2:wght@500;800&display=swap" rel="stylesheet">
</head>
<body class="home">
<div id="wrap">
  <div id="header"> <img src="images/logo_admin.png" />
    <div id="nav">
      <ul class="menu">
        <li><a href="admin_table.php">Таблица</a></li>
        <li><a href="admin.php">Добавить</a></li>
        <li><a href="update.php">Изменить</a></li>
        <li><a href="delete.php">Удалить</a></li>
      </ul>
    </div>
    <!--end nav-->
  </div>
  <!--end header-->
  <div id="prokrutka">
    <table>
      <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Genre</th>
        <th>Album</th>
        <th>Composer</th>
        <th>Age</th>
        <th>Text</th>
      </tr>
      <?php
      foreach ($result as $songs) {
      ?>
      <tr>
        <td>
          <?php
            echo ($songs['id'])
            ?>
            </td>
        <td><?php
            echo ($songs['name'])
            ?></td>
        <td><?php
            echo ($songs['genre'])
            ?></td>
        <td><?php
            echo ($songs['album'])
            ?></td>
        <td><?php
            echo ($songs['composer'])
            ?></td>
        <td><?php
            echo ($songs['age'])
            ?></td>
        <td><?php
            echo ($songs['text'])
            ?></td>
      </tr>
      <?php
      }
      ?>
    </table>
  </div>
  <!--end featured-section-->
</div>
<!--end wrap-->
</body>
<div class="cache-images"><img src="images/red-button-bg.png" width="0" height="0" alt="" /><img src="images/black-button-bg.png" width="0" height="0" alt="" /></div>
<!--end cache-images-->
</html>