<?php
include("app/controlers/controler.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Akono | Project</title>
<meta charset="utf-8">
<link type="text/css" rel="stylesheet" href="styles/style.css" />
<script type="text/javascript" src="js/audio.js"></script>
<!--[if IE 6]>
<script src="js/ie6-transparency.js"></script>
<script>DD_belatedPNG.fix('#header img, #featured-section h2, #circles img, #frontpage-sidebar .read-more, .blue-bullets li, #sidebar .sidebar-button, #project-content .read-more, .more-link, #contact-form .submit, .jcarousel-skin-tango .jcarousel-next-horizontal, .jcarousel-skin-tango .jcarousel-prev-horizontal, #commentform .submit');</script>
<style>body { behavior: url("styles/ie6-hover-fix.htc"); }</style>
<link rel="stylesheet" href="styles/ie6.css" />
<![endif]-->
<!--[if IE 7]><link rel="stylesheet" href="styles/ie7.css" /><![endif]-->
<!--[if IE 8]><link rel="stylesheet" href="styles/ie8.css" /><![endif]-->
</head>
<body class="page">
<div id="wrap">
  <div id="header"> <img src="images/logo.png" />
    <div id="nav">
      <ul class="menu">
        <li><a href="index.php">Главная</a></li>
        <li><a href="page.php">Жанры</a></li>
        <li><a href="portfolio.php">Исполнители</a></li>
        <li><a href="articles.php">Альбомы</a></li>
        <li><a href="contact.php">Композиции</a></li>
      </ul>
    </div>
    <!--end nav-->
  </div>
  <!--end header-->
  <div class="page-headline">Исполнитель</div>
  <div id="main">

    <!--end sidebar-->
    <div id="content">
      <div class="post" >
        <div class="page-headline"><?php echo $_GET['composer'] ?></div>
      </div>
      <!--end post-->
    </div>
    <!--end content-->
  </div>
  <div id="footer">
    <div class="page-headline">Композиции</div>
    <h3 class="post-meta"></h3>
    <?php
        if (empty($result)) {
        ?>
        <h1 style="font-size: 30px; padding-top: 10px; padding-left: 300px;">
                Ни один трек не найден
            </h1>
            <?php   } else {
            foreach ($result as $songs) {
            ?>
    <a href="<?php echo 'track.php'.'?id=' . $songs['id']?>"><?php echo $songs['name']?></a>
    <audio id="song" width="300" height="32" ontimeupdate="updateTime()" controls="controls">
      <source src="<?php echo "audio/". $songs['name'] . '.mp3'?>" type="audio/mp3" />
    </audio>
    <?php } ?>
    <?php } ?>
  </div>
  <!--end main-->
</div>
<!--end wrap-->
</body>
<div class="cache-images"><img src="images/red-button-bg.png" width="0" height="0" alt="" /><img src="images/black-button-bg.png" width="0" height="0" alt="" /></div>
<!--end cache-images-->
</html>