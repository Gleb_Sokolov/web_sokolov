<?php
include("app/database/db.php");
define("ROOT_PATH", realpath(dirname(__FILE__)))
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>MusoTeka</title>
    <link rel="stylesheet" href="styles/style.css">
    <link rel="preconnect" href="https://fonts.googleapis.com"><link rel="preconnect" href="https://fonts.gstatic.com" crossorigin><link href="https://fonts.googleapis.com/css2?family=Exo+2:wght@500;800&display=swap" rel="stylesheet">
</head>
<body class="home">
<?php
if(array_key_exists('button1', $_POST) && $_POST['id'] != '') {
        $id = $_POST['id'];

        delete('songs', $id);
}
?>
<div id="wrap">
  <div id="header"> <img src="images/logo_admin.png" />
    <div id="nav">
      <ul class="menu">
        <li><a href="admin_table.php">Таблица</a></li>
        <li><a href="admin.php">Добавить</a></li>
        <li><a href="update.php">Изменить</a></li>
        <li><a href="delete.php">Удалить</a></li>
      </ul>
    </div>
    <!--end nav-->
  </div>
  <!--end header-->
  <div id="featured-section">
    <div class="text-field">
      <h3 class="post-meta">Удалить трек</div>

      <form method="post" enctype="multipart/form-data">
        <p><input class="text-field__input" type="text" name="id" placeholder="ID трека" /></p>
        <p><input type="submit" name="button1" class="button" value="Удалить" /></p>

      </form>
    </div>
  </div>
  <!--end featured-section-->
</div>
<!--end wrap-->
</body>
</html>