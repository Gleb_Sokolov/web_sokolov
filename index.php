<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>MusoTeka</title>
    <link rel="stylesheet" href="styles/style.css">
    <link rel="stylesheet" href="styles/skin.css">
    <link rel="preconnect" href="https://fonts.googleapis.com"><link rel="preconnect" href="https://fonts.gstatic.com" crossorigin><link href="https://fonts.googleapis.com/css2?family=Exo+2:wght@500;800&display=swap" rel="stylesheet">
</head>
<body class="home">
<div id="wrap">
  <div id="header"> <img src="images/logo.png" />
    <div id="nav">
      <ul class="menu">
        <li><a href="index.php">Главная</a></li>
        <li><a href="page.php">Жанры</a></li>
        <li><a href="portfolio.php">Исполнители</a></li>
        <li><a href="articles.php">Альбомы</a></li>
        <li><a href="contact.php">Композиции</a></li>
      </ul>
    </div>
    <!--end nav-->
  </div>
  <!--end header-->
  <div id="featured-section">
    <div id="circles"> <img class="first" src="images/image 2.png" /> </div>
    <!--end image-slider-->
  </div>
  <!--end featured-section-->
  <div id="frontpage-main">
    <div id="frontpage-content">
      <h3>Более 1000......и т.д.</h3>
      <p>На нашем сайте размещаются огромное количество треков:</p>
      <ul class="blue-bullets">
        <li>10+ жанров</li>
        <li>1000+ композиторов</li>
        <li>100000+ альбомов</li>
        <li>Вы точно найдёте то, что вам понравится</li>
      </ul>
      
    </div>
    <!--end frontpage-sidebar-->
  </div>
  
  <!--end frontpage-main-->
</div>
<!--end wrap-->
</body>
<div class="cache-images"><img src="images/red-button-bg.png" width="0" height="0" alt="" /><img src="images/black-button-bg.png" width="0" height="0" alt="" /></div>
<!--end cache-images-->
</html>