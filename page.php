<?php
include("app/controlers/listgenre.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>MusoTeka | Genres</title>
<meta charset="utf-8">
<link type="text/css" rel="stylesheet" href="styles/style.css" />
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
<!--[if IE 6]>
<script src="js/ie6-transparency.js"></script>
<script>DD_belatedPNG.fix('#header img, #featured-section h2, #circles img, #frontpage-sidebar .read-more, .blue-bullets li, #sidebar .sidebar-button, #project-content .read-more, .more-link, #contact-form .submit, .jcarousel-skin-tango .jcarousel-next-horizontal, .jcarousel-skin-tango .jcarousel-prev-horizontal, #commentform .submit');</script>
<style>body { behavior: url("styles/ie6-hover-fix.htc"); }</style>
<link rel="stylesheet" href="styles/ie6.css" />
<![endif]-->
<!--[if IE 7]><link rel="stylesheet" href="styles/ie7.css" /><![endif]-->
<!--[if IE 8]><link rel="stylesheet" href="styles/ie8.css" /><![endif]-->
</head>
<?php include("app/include/header.php"); ?>
  <div class="page-headline">Жанры</div>
  <div id="main">
    <div id="porfolio-content">
      <?php
        if (empty($result)) {
        ?>
        <h1 style="font-size: 30px; padding-top: 10px; padding-left: 300px;">
                Ни один трек не найден
            </h1>
            <?php   } else {
            foreach ($result as $songs) {
            ?>
        <div class="portfolio-item"> <a href="<?php echo 'genres.php'.'?genre='. $songs['genre']?>"><img width="195" height="50" src="<?php echo 'images/'. $songs['genre']. '.png'?>" alt="" /></a> </div>
        <!--end portfolio-item-->
        </audio>
        <?php } ?>
    <?php } ?>
    </div>
    <!--end sidebar-->
  </div>
  <!--end main-->
</div>
<!--end wrap-->
</body>
<div class="cache-images"><img src="images/red-button-bg.png" width="0" height="0" alt="" /><img src="images/black-button-bg.png" width="0" height="0" alt="" /></div>
<!--end cache-images-->
</html>