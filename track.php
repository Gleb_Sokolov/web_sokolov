<?php
include("app/controlers/track_info.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Akono | Project</title>
<meta charset="utf-8">
<link type="text/css" rel="stylesheet" href="styles/style.css" />
<!--[if IE 6]>
<script src="js/ie6-transparency.js"></script>
<script>DD_belatedPNG.fix('#header img, #featured-section h2, #circles img, #frontpage-sidebar .read-more, .blue-bullets li, #sidebar .sidebar-button, #project-content .read-more, .more-link, #contact-form .submit, .jcarousel-skin-tango .jcarousel-next-horizontal, .jcarousel-skin-tango .jcarousel-prev-horizontal, #commentform .submit');</script>
<style>body { behavior: url("styles/ie6-hover-fix.htc"); }</style>
<link rel="stylesheet" href="styles/ie6.css" />
<![endif]-->
<!--[if IE 7]><link rel="stylesheet" href="styles/ie7.css" /><![endif]-->
<!--[if IE 8]><link rel="stylesheet" href="styles/ie8.css" /><![endif]-->
</head>
<?php include("app/include/header.php"); ?>
  <div class="page-headline">Композиция</div>
  <div id="main">
    <div id="sidebar">
      <div class="portfolio-item first"><img width="200" height="200" src="<?php echo "images/album/". $result['album'] . '.png'?>" alt="" /> </div>
          <!--end portfolio-item-->
    </div>
    <!--end sidebar-->
    <div id="content">
      <div class="post" >
        <div class="page-headline"><?php echo $result['name'] ?></div>
        <h3 class="post-meta"><?php echo $result['composer'] ?></h3>
        <h3 class="post-meta"></h3>
        <p><?php echo $result['age'] ?></p>
        <p>Альбом - <?php echo $result['album'] ?></p>
        <p>Жанр - <?php echo $result['genre'] ?></p>
      </div>
      <!--end post-->
    </div>
    <!--end content-->
  </div>
  <div id="content">
    <p>Аудио:</p>
    <audio id="song" width="300" height="32" ontimeupdate="updateTime()" controls="controls">
      <source src="<?php echo "audio/". $result['name'] . '.mp3'?>" type="audio/mp3" />
    </audio>
  </div>
  <div id="footer">
    <div class="page-headline">Текст песни:</div>
    <h3 class="post-meta"></h3>
      <div id="prokrutka">
        <p><?php echo $result['text'] ?></p>
      </div>
  </div>
  <!--end main-->
</div>
<!--end wrap-->
</body>
<div class="cache-images"><img src="images/red-button-bg.png" width="0" height="0" alt="" /><img src="images/black-button-bg.png" width="0" height="0" alt="" /></div>
<!--end cache-images-->
</html>