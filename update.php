<?php
include("app/database/db.php");
define("ROOT_PATH", realpath(dirname(__FILE__)))
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>MusoTeka</title>
    <link rel="stylesheet" href="styles/style.css">
    <link rel="preconnect" href="https://fonts.googleapis.com"><link rel="preconnect" href="https://fonts.gstatic.com" crossorigin><link href="https://fonts.googleapis.com/css2?family=Exo+2:wght@500;800&display=swap" rel="stylesheet">
</head>
<body class="home">
<?php
if(array_key_exists('button1', $_POST) && $_POST['id'] != '') {
        $id = $_POST['id'];
        $post = array();
        if($_POST['name'] != ''){
          $name = $_POST['name'];
          $post['name'] = $name;
        }
        if($_POST['genre'] != ''){
          $genre = $_POST['genre'];
          $post['genre'] = $genre;
        }
        if($_POST['album'] != ''){
          $album = $_POST['album'];
          $post['album'] = $album;
        };
        if($_POST['composer'] != ''){
          $composer = $_POST['composer'];
          $post['composer'] = $composer;
        };
        if($_POST['age'] != ''){
          $age = $_POST['age'];
          $post['age'] = $age;
        };
        if($_POST['text'] != ''){
          $text = $_POST['text'];
          $post['text'] = $text;
        };

        update('songs', $id, $post);

        if(!empty($_FILES['genre_png']['name'])){
          $genreTmpName = $_FILES['genre_png']['tmp_name'];
          $genreDestination = ROOT_PATH . "\images\\" . $genre . '.png';
          $result = move_uploaded_file($genreTmpName, $genreDestination);
        }
        if(!empty($_FILES['composer_png']['name'])){
          $composerTmpName = $_FILES['composer_png']['tmp_name'];
          $composerDestination = ROOT_PATH . "\images\composer\\" . $composer . '.png';
          $result = move_uploaded_file($composerTmpName, $composerDestination);
        }
        if(!empty($_FILES['album_png']['name'])){
          $albumTmpName = $_FILES['album_png']['tmp_name'];
          $albumDestination = ROOT_PATH . "\images\album\\" . $album . '.png';
          $result = move_uploaded_file($albumTmpName, $albumDestination);
        }
        if(!empty($_FILES['music']['name'])){
          $musicTmpName = $_FILES['music']['tmp_name'];
          $musicDestination = ROOT_PATH . "\audio\\" . $name . '.mp3';
          $result = move_uploaded_file($musicTmpName, $musicDestination);
        }
}
?>
<div id="wrap">
  <div id="header"> <img src="images/logo_admin.png" />
    <div id="nav">
      <ul class="menu">
        <li><a href="admin_table.php">Таблица</a></li>
        <li><a href="admin.php">Добавить</a></li>
        <li><a href="update.php">Изменить</a></li>
        <li><a href="delete.php">Удалить</a></li>
      </ul>
    </div>
    <!--end nav-->
  </div>
  <!--end header-->
  <div id="featured-section">
    <div class="text-field">
      <h3 class="post-meta">Изменить трек</div>

      <form method="post" enctype="multipart/form-data">
        <p><input class="text-field__input" type="text" name="id" placeholder="ID трека" /></p>
        <p><input class="text-field__input" type="text" name="name" placeholder="Имя трека" /></p>
        <p><input class="text-field__input" type="text" name="genre" placeholder="Название жанра" /></p>
        <p><input class="text-field__input" type="text" name="album" placeholder="Название альбома" /></p>
        <p><input class="text-field__input" type="text" name="composer" placeholder="Название исполнителя" /></p>
        <p><input class="text-field__input" type="number" name="age" placeholder="Год" /></p>
        <p><input class="text-field__input" type="text" name="text" placeholder="Текст песни" /></p>
        <p><label for="genre_png">Фото жанра:</label>
        <input type="file" name="genre_png" accept=".png"></p>
        <p><label for="composer_png">Фото исполнителя:</label>
        <input type="file" name="composer_png" accept=".png"></p>
        <p><label for="album_png">Фото альбома:</label>
        <input type="file" name="album_png" accept=".png"></p>
        <p><label for="music">Музыкальный файл:</label>
        <input type="file" name="music" accept=".mp3"></p>
        <p><input type="submit" name="button1" class="button" value="Изменить" /></p>

      </form>
    </div>
  </div>
  <!--end featured-section-->
</div>
<!--end wrap-->
</body>
</html>